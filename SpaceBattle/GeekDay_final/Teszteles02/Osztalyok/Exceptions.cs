﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teszteles02
{
    class NemEgyertelmuHajo : ApplicationException
    {
        public int NumberOfUnits { get; set; }
        public NemEgyertelmuHajo(int NumberOfUnits)
        {
            this.NumberOfUnits = NumberOfUnits;
        }
    }
    class NemTalalhatoHajo : ApplicationException { }

    class TobbAzonosItemIDJottAt : ApplicationException { }
}