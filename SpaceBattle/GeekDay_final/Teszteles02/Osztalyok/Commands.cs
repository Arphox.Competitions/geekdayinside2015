﻿using SpaceBattle.Common;

namespace Teszteles02
{
    static class Commands
    {
        public static BattleCommand Split(int ItemId, int NumberOfUnits)
        {
            CmdSplit split = new CmdSplit();
            split.ItemId = ItemId;
            split.NumberOfUnits = NumberOfUnits;
            return split;
        }
        public static BattleCommand Move(int ItemId, float TargetX, float TargetY)
        {
            CmdMove move = new CmdMove();
            move.ItemId = ItemId;
            move.TargetX = TargetX;
            move.TargetY = TargetY;
            return move;
        }
        public static BattleCommand Shoot(int ItemId, int NumberOfUnits, int OtherItemId)
        {
            CmdShoot shoot = new CmdShoot();
            shoot.ItemId = ItemId;
            shoot.NumberOfUnits = NumberOfUnits;
            shoot.OtherItemId = OtherItemId;
            return shoot;
        }
    }
}