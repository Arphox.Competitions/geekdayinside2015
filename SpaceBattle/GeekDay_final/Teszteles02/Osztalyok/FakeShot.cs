﻿using System.Collections.Generic;
using System.Linq;
using SpaceBattle.Common;
using System;

namespace Teszteles02
{
    class FakeShot
    {
        public OwnPlanet SourcePlanet { get; private set; }
        public GameItem TargetPlanet { get; private set; }
        public OwnShip Ship { get; private set; }
        public int NumberOfUnits { get; private set; }

        public FakeShotState State { get; set; }

        public FakeShot(OwnPlanet SourcePlanet, GameItem TargetPlanet, int NumberOfUnits)
        {
            this.SourcePlanet = SourcePlanet;
            this.TargetPlanet = TargetPlanet;
            this.NumberOfUnits = NumberOfUnits;
            NaplozHaKell();
        }

        public BattleCommand GetNextCommand(List<OwnShip> OwnShips)
        {
            #region ShipCalledBack => Ha helyben van, illesztés
            if (State == FakeShotState.ShipCalledBack)
            {
                //Létezik-e még a Shipünk?
                IEnumerable<OwnShip> matchingShips = (from x in OwnShips where x.ItemId == Ship.ItemId select x);
                if (matchingShips.Count() != 1) //Ha nem találjuk a hajónkat, valószínűleg kilőtték
                {
                    State = FakeShotState.Error;
                    return new CmdNop();
                }
                //Ship-Update
                Ship = matchingShips.Single();

                //Ha áll a hajónk, akkor visszaérkezett = ShipArived
                if (Ship.DestinationX < 0 && Ship.DestinationY < 0)
                {
                    State++;
                    NaplozHaKell();
                    return Commands.Shoot(Ship.ItemId, NumberOfUnits, SourcePlanet.ItemId);
                }
            }
            #endregion
            #region ShipSent => Ha helyben van, visszahívás
            if (State == FakeShotState.ShipSent)
            {
                IEnumerable<OwnShip> matchingShips = (from x in OwnShips where x.ItemId == Ship.ItemId select x);
                //Létezik-e még a Shipünk?
                if (matchingShips.Count() != 1) //Ha nem találjuk a hajónkat, valószínűleg időközben kilőtték
                {
                    State = FakeShotState.Error;
                    return new CmdNop();
                }
                //Ship-Update
                Ship = matchingShips.Single();

                //Ha áll a hajónk, akkor támadhatunk vele
                if (Ship.DestinationX < 0 && Ship.DestinationY < 0)
                {
                    State++;
                    NaplozHaKell();
                    return Commands.Move(Ship.ItemId, SourcePlanet.PosX, SourcePlanet.PosY);
                }
            }
            #endregion
            #region ShipCreated => Kiküldés
            if (State == FakeShotState.ShipCreated)
            {
                //Próbáljuk meg kitalálni, hogy melyik hajó jött létre:
                IEnumerable<OwnShip> matchingShips = from x in OwnShips
                                                     //biztos, hogy a kiküldött planet koordinátáján van először
                                                     where x.PosX == SourcePlanet.PosX && x.PosY == SourcePlanet.PosY
                                                     //biztos, hogy ugyanakkora, mint amekkorát mi leválasztottunk
                                                     && x.NumberOfUnits == NumberOfUnits
                                                     && x.DestinationX < 0 && x.DestinationY < 0
                                                     select x;
                if (matchingShips.Count() == 0)
                {
                    State = FakeShotState.Error;
                    throw new NemTalalhatoHajo();
                }
                if (matchingShips.Count() > 1) //Ha több ilyen hajót találtunk, szopás
                {
                    State = FakeShotState.Error;
                    throw new NemEgyertelmuHajo(NumberOfUnits);
                }
                else  //De csak ha egyet, akkor egyértelmű, hogy melyik hajót fogjuk küldözgetni
                {
                    Ship = matchingShips.First();
                }

                State++;
                NaplozHaKell();
                return Commands.Move(Ship.ItemId, TargetPlanet.PosX, TargetPlanet.PosY);
            }
            #endregion
            #region Initial => Leválasztás
            if (State == FakeShotState.Initial)
            {
                State++;
                NaplozHaKell();
                return Commands.Split(SourcePlanet.ItemId, NumberOfUnits);
            }
            #endregion
            return new CmdNop();
        }

        private void NaplozHaKell()
        {
            if (Settings.Naplozas.FAKESHOTS)
            {
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(Settings.FilePaths.CommandsTXT, true))
                {
                    sw.WriteLine(ToString());
                }
            }
        }
        public override string ToString()
        {
            if (Ship != null)
                return String.Format("FakeShot: ({0},{1})->({2},{3}) State={4}, (Ship:{5},NoU:{6})",
                SourcePlanet.PosX, SourcePlanet.PosY, TargetPlanet.PosX, TargetPlanet.PosY,
                State, Ship.ItemId, NumberOfUnits);
            else
                return String.Format("FakeShot: ({0},{1})->({2},{3}) State={4}, (Ship:null,NoU:{5})",
                SourcePlanet.PosX, SourcePlanet.PosY, TargetPlanet.PosX, TargetPlanet.PosY,
                State, NumberOfUnits);
        }

        //STATIC:
        /// <summary>
        /// Kitörli a véget ért és hibába futott lövéseket.
        /// </summary>
        /// <param name="transferShots"></param>
        public static void Takaritas(List<FakeShot> FakeShots)
        {
            for (int i = FakeShots.Count - 1; i >= 0; i--)
            {
                if (FakeShots[i].State == FakeShotState.Done || FakeShots[i].State == FakeShotState.Error)
                {
                    FakeShots.Remove(FakeShots[i]);
                }
            }
        }
    }

    enum FakeShotState
    {
        Initial, ShipCreated, ShipSent, ShipCalledBack, Done, Error
    }
}