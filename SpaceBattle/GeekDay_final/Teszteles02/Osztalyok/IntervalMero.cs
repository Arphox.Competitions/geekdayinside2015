﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;

namespace Teszteles02
{
    static class IntervalMero
    {
        static bool NAPLOZ = true;

        static Stopwatch sw = new Stopwatch();
        static List<long> ElapsedTimes = new List<long>();

        public static void Mer()
        {
            if (AI2.Orajel % 3 == 0)
            {
                ElapsedTimes.Add(sw.ElapsedMilliseconds);
                Settings.INTERVAL = Convert.ToSingle(ElapsedTimes.Average());

                if (NAPLOZ)
                {
                    using (StreamWriter writer = new StreamWriter(Settings.FilePaths.IntervalsTXT, true))
                    {
                        writer.WriteLine(Settings.INTERVAL);
                    }
                }
            }

            if (AI2.Orajel % 2 == 1)
                sw.Restart();
            else if (AI2.Orajel % 2 == 0)
                sw.Stop();
        }
    }
}