﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teszteles02
{
    static class ExtensionMethods
    {
        public static int OsszesNoU(this List<OwnPlanet> OwnPlanets)
        {
            int db = 0;
            for (int i = 0; i < OwnPlanets.Count; i++)
                db += OwnPlanets[i].NumberOfUnits;
            return db;
        }

        public static OwnPlanet Legnagyobb(this List<OwnPlanet> OwnPlanets)
        {
            int max = OwnPlanets.Max(y => y.NumberOfUnits);
            return (from x in OwnPlanets
                    where x.NumberOfUnits == max
                    select x).First();
        }
        public static OwnPlanet LegnagyobbAmiNem(this List<OwnPlanet> OwnPlanets, OwnPlanet ez)
        {
            var max1 = OwnPlanets.Where(x => x.ItemId != x.ItemId);
            if (max1.Count() == 0)
                return null;

            int max = max1.Max(y => y.NumberOfUnits);
            var q = (from x in OwnPlanets
                     where x.NumberOfUnits == max
                     select x);
            if (q.Count() == 0)
                return null;
            else
                return q.First();
        }
        public static OwnPlanet LegkozelebbiAmiNagyobbMint(this List<OwnPlanet> OwnPlanets, GameItem kozelMihez, int mennyinel)
        {
            if (OwnPlanets == null || OwnPlanets.Count == 0)
                return null;
            List<OwnPlanet> nagyobbak = OwnPlanets.Where(x => x.NumberOfUnits > mennyinel).ToList();

            if (nagyobbak.Count == 0)
                return null;

            int mini = 0;
            double minDist = double.MaxValue;

            for (int i = 1; i < nagyobbak.Count; i++)
            {
                double aktDist = Matek.KetPontTavolsaga(nagyobbak[i].PosX, nagyobbak[i].PosY, kozelMihez.PosX, kozelMihez.PosY);
                if (aktDist < minDist)
                {
                    minDist = aktDist;
                    mini = i;
                }
            }

            return nagyobbak[mini];
        }
        public static OwnPlanet LegkozelebbiAmiNagyobbMintEsNincsBenne(this List<OwnPlanet> OwnPlanets, GameItem micsoda, List<PointF> koordinatak)
        {
            int mennyinel = micsoda.NumberOfUnits;

            if (OwnPlanets == null || OwnPlanets.Count == 0)
                return null;
            List<OwnPlanet> nagyobbak = OwnPlanets
                .Where(x => x.NumberOfUnits > mennyinel
                            && !koordinatak.Contains(new PointF(x.PosX, x.PosY)))
                            .ToList();

            if (nagyobbak.Count == 0)
                return null;

            int mini = 0;
            double minDist = double.MaxValue;

            for (int i = 1; i < nagyobbak.Count; i++)
            {
                double aktDist = Matek.KetPontTavolsaga(nagyobbak[i].PosX, nagyobbak[i].PosY, micsoda.PosX, micsoda.PosY);
                if (aktDist < minDist)
                {
                    minDist = aktDist;
                    mini = i;
                }
            }

            return nagyobbak[mini];
        }

        public static List<OwnPlanet> NagyobbakMint(this List<OwnPlanet> items, int Meret)
        {
            return (from x in items
                    where x.NumberOfUnits > Meret
                    select x).ToList();
        }

        public static List<EnemyShip> Veszelyesek(this List<EnemyShip> items, List<OwnPlanet> OwnPlanets)
        {
            List<PointF> OwnPlanetPositions = new List<PointF>();
            for (int i = 0; i < OwnPlanets.Count; i++)
                OwnPlanetPositions.Add(new PointF(OwnPlanets[i].PosX, OwnPlanets[i].PosY));


            List<EnemyShip> veszelyesek = new List<EnemyShip>();
            PointF aktItemDest;

            for (int i = 0; i < items.Count; i++)
            {
                aktItemDest = new PointF(items[i].DestinationX, items[i].DestinationY);
                if (OwnPlanetPositions.Contains(aktItemDest))
                    veszelyesek.Add(items[i]);
            }


            return veszelyesek;
        }

    }
}