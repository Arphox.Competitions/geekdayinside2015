﻿using System.Collections.Generic;
using System.Linq;

namespace Teszteles02
{
    static class PlanetMap
    {
        public static bool Initialized { get; private set; }

        static GameItem[,] elements = new GameItem[Settings.MapSizeX, Settings.MapSizeY];
        /// <summary>
        /// A mapon lévő bolygók.
        /// </summary>
        public static GameItem[,] Elements
        {
            get { return elements; }
        }

        //Constructor
        public static void Initialize(List<OwnPlanet> OwnPlanets, List<EnemyPlanet> EnemyPlanets, List<NeutralPlanet> NeutralPlanets)
        {
            //items beillesztése
            for (int i = 0; i < OwnPlanets.Count; i++)
            {
                OwnPlanet item = OwnPlanets[i];
                elements[(int)item.PosX, (int)item.PosY] = item;
            }

            //EnemyPlanets beillesztése
            for (int i = 0; i < EnemyPlanets.Count; i++)
            {
                EnemyPlanet item = EnemyPlanets[i];
                elements[(int)item.PosX, (int)item.PosY] = item;
            }

            //NeutralPlanetsList beillesztése
            for (int i = 0; i < NeutralPlanets.Count; i++)
            {
                NeutralPlanet item = NeutralPlanets[i];
                elements[(int)item.PosX, (int)item.PosY] = item;
            }

            PermaNeutralPlanets = NeutralPlanets;
            PermaEnemyPlanets = EnemyPlanets;

            Initialized = true;
        }
        public static List<NeutralPlanet> PermaNeutralPlanets { get; private set; }
        public static List<EnemyPlanet> PermaEnemyPlanets { get; private set; }

        //Public Methods
        /// <summary>
        /// Aktualizálja a planetMapot.
        /// </summary>
        /// <param name="newMap"></param>
        public static void Refresh(List<OwnPlanet> OwnPlanets, List<EnemyPlanet> EnemyPlanets, List<NeutralPlanet> NeutralPlanets)
        {
            //items beillesztése
            for (int i = 0; i < OwnPlanets.Count; i++)
            {
                OwnPlanet item = OwnPlanets[i];
                elements[(int)item.PosX, (int)item.PosY] = item;
            }

            //EnemyPlanets beillesztése
            for (int i = 0; i < EnemyPlanets.Count; i++)
            {
                EnemyPlanet item = EnemyPlanets[i];
                elements[(int)item.PosX, (int)item.PosY] = item;
            }

            //NeutralPlanetsList beillesztése
            for (int i = 0; i < NeutralPlanets.Count; i++)
            {
                NeutralPlanet item = NeutralPlanets[i];
                elements[(int)item.PosX, (int)item.PosY] = item;
            }

            //PermaNeutralPlanetList beillesztése
            for (int i = 0; i < NeutralPlanets.Count; i++)
            {
                if (PermaNeutralPlanets.Contains(NeutralPlanets[i]))
                {
                    PermaNeutralPlanets[PermaNeutralPlanets.IndexOf(NeutralPlanets[i])] = NeutralPlanets[i];
                }
                else
                {
                    PermaNeutralPlanets.Add(NeutralPlanets[i]);
                }
            }
            for (int i = PermaNeutralPlanets.Count - 1; i >= 0 ; i--)
			{
                if (EnemyPlanets.Where(x => x.ItemId == PermaNeutralPlanets[i].ItemId).Count() == 1)
                {
                    PermaNeutralPlanets.RemoveAt(i);
                }
            }

            //PermaEnemyPlanetList beillesztése
            for (int i = 0; i < EnemyPlanets.Count; i++)
            {
                if (PermaEnemyPlanets.Contains(EnemyPlanets[i]))
                {
                    PermaEnemyPlanets[PermaEnemyPlanets.IndexOf(EnemyPlanets[i])] = EnemyPlanets[i];
                }
                else
                {
                    PermaEnemyPlanets.Add(EnemyPlanets[i]);
                }
            }
        }

        /// <summary>
        /// Kiírja egy fájlba a PlanetMap aktuális tartalmát.
        /// </summary>
        /// <param name="fileName"></param>
        public static void FajlbaIr(string fileName)
        {
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(fileName))
            {
                for (int i = 0; i < elements.GetLength(0); i++)
                {
                    for (int j = 0; j < elements.GetLength(1); j++)
                    {
                        if (elements[j, i] is NeutralPlanet)
                            sw.Write("N");
                        else if (elements[j, i] is EnemyPlanet)
                            sw.Write("E");
                        else if (elements[j, i] is OwnPlanet)
                            sw.Write("O");
                        else
                            sw.Write("-");
                    }
                    sw.WriteLine();
                }
            }
        }
    }
}