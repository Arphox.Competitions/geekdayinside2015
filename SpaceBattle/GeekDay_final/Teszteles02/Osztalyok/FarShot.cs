﻿using System;
using SpaceBattle.Common;
using System.Linq;
using System.Collections.Generic;

namespace Teszteles02
{
    /// <summary>
    /// Távolsági lövések leadására szakosodott osztály
    /// </summary>
    class FarShot
    {
        public OwnPlanet SourcePlanet { get; private set; }
        public GameItem TargetPlanet { get; private set; }
        public OwnShip Ship { get; private set; }
        public int NumberOfUnits { get; private set; }

        public ShotState State { get; set; }

        public FarShot(OwnPlanet SourcePlanet, GameItem TargetPlanet, int NumberOfUnits)
        {
            this.SourcePlanet = SourcePlanet;
            this.TargetPlanet = TargetPlanet;
            this.NumberOfUnits = NumberOfUnits;
            NaplozHaKell();
        }

        public BattleCommand Activate(List<OwnShip> OwnShips)
        {
            if (State == ShotState.ShipOnPlace)
            {
                IEnumerable<OwnShip> matchingShips = (from x in OwnShips where x.ItemId == Ship.ItemId select x);
                //Létezik-e még a Shipünk?
                if (matchingShips.Count() != 1) //Ha nem találjuk a hajónkat
                {
                    State = ShotState.Error;
                    return new CmdNop();
                }
                //Ship-Update
                Ship = (from x in OwnShips where x.ItemId == Ship.ItemId select x).Single();

                State++;
                NaplozHaKell();

                return Commands.Shoot(Ship.ItemId, Ship.NumberOfUnits, TargetPlanet.ItemId);
            }
            if (State == ShotState.ShipSent)
            {
                IEnumerable<OwnShip> matchingShips = (from x in OwnShips where x.ItemId == Ship.ItemId select x);
                //Létezik-e még a Shipünk?
                if (matchingShips.Count() != 1) //Ha nem találjuk a hajónkat
                {
                    State = ShotState.Error;
                    return new CmdNop();
                }
                //Ship-Update
                Ship = matchingShips.Single();

                //Ha áll a hajónk, akkor támadhatunk vele
                if (Ship.DestinationX < 0 && Ship.DestinationY < 0)
                {
                    State++;
                    NaplozHaKell();
                }
            }
            //Ha már helyben vagyunk, valószínűleg sikerülni fog a szinte azonnal leadott lövés
            if (State == ShotState.ShipCreated)
            {
                //Próbáljuk meg kitalálni, hogy melyik hajó jött létre:
                IEnumerable<OwnShip> matchingShips = from x in OwnShips
                                                     //biztos, hogy a kiküldött planet koordinátáján van először
                                                     where x.PosX == SourcePlanet.PosX && x.PosY == SourcePlanet.PosY
                                                         //biztos, hogy ugyanakkora, mint amekkorát mi leválasztottunk
                                                     && x.NumberOfUnits == NumberOfUnits
                                                     && x.DestinationX < 0 && x.DestinationY < 0
                                                     select x;
                if (matchingShips.Count() == 0)
                {
                    State = ShotState.Error;
                    //throw new NemTalalhatoHajo();
                    return new CmdNop();
                }
                if (matchingShips.Count() > 1) //Ha több ilyen hajót találtunk, szopás
                {
                    State = ShotState.Error;
                    //throw new NemEgyertelmuHajo(NumberOfUnits);
                    return new CmdNop();
                }
                else  //De csak ha egyet, akkor egyértelmű, hogy melyik hajót fogjuk küldözgetni
                {
                    Ship = matchingShips.First();
                }

                State++;
                NaplozHaKell();
                return Commands.Move(Ship.ItemId, TargetPlanet.PosX, TargetPlanet.PosY);
            }
            if (State == ShotState.Initial)
            {
                State++;
                NaplozHaKell();
                return Commands.Split(SourcePlanet.ItemId, NumberOfUnits);
            }
            return new CmdNop();
        }

        private void NaplozHaKell()
        {
            if (Settings.Naplozas.FARSHOTS)
            {
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(Settings.FilePaths.CommandsTXT, true))
                {
                    sw.WriteLine(ToString());
                }
            }
        }

        public override string ToString()
        {
            if (Ship != null)
                return String.Format("FarShot: ({0},{1})->({2},{3}) State={4}, (Ship:{5},NoU:{6})",
                SourcePlanet.PosX, SourcePlanet.PosY, TargetPlanet.PosX, TargetPlanet.PosY,
                State, Ship.ItemId, NumberOfUnits);
            else
                return String.Format("FarShot: ({0},{1})->({2},{3}) State={4}, (Ship:null,NoU:{5})",
                SourcePlanet.PosX, SourcePlanet.PosY, TargetPlanet.PosX, TargetPlanet.PosY,
                State, NumberOfUnits);
        }

        //STATIC:
        /// <summary>
        /// Kitörli a véget ért és hibába futott lövéseket.
        /// </summary>
        /// <param name="transferShots"></param>
        public static void Takaritas(List<FarShot> farShots)
        {
            for (int i = farShots.Count - 1; i >= 0; i--)
            {
                if (farShots[i].State == ShotState.Done || farShots[i].State == ShotState.Error)
                {
                    farShots.Remove(farShots[i]);
                }
            }
        }
    }

    enum ShotState
    {
        Initial, ShipCreated, ShipSent, ShipOnPlace, Done, Error
    }
}