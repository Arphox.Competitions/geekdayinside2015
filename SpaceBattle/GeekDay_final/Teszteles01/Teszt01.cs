﻿using SpaceBattle.Common;
using System.Collections.Generic;
using System.Drawing;

namespace Teszteles01
{
    public class Teszt01 : IBattleClient
    {
        //=======================================================================================================
        //Gyakran hívott metódusok, a hívás sorrendjében.
        public void GiveRemainingTimeToClient(int seconds)
        {
            AI1.Orajel++;
            Settings.RemainingTime = seconds;
        }
        public void GiveGameItemsToClient(List<GameItemDescriptor> gameItems)
        {
            IntervalMero.Mer();
            AI1.Adatfeldolgozas.Feldolgozas(gameItems);
        }
        public List<BattleCommand> GetCommandsFromClient()
        {
            return AI1.KiadottParancsok();
        }
        //=======================================================================================================


        //Egyszer, az elején hívott metódusok:
        public string ClientName
        {
            get
            {
                return Settings.ClientName;
            }
        }
        public Brush ClientBrush
        {
            get { return Settings.ClientBrush; }
        }
        public void GiveMapSizeToClient(int size_x, int size_y)
        {
            Settings.MapSizeX = size_x;
            Settings.MapSizeY = size_y;
        }

        //Alkalmanként hívott metódus:
        public void GiveMessageToClient(string msg)
        {
            /// "GAME OVER"
            /// "REMAINING TIME: " + currentTime
            /// "LOGIN FORBIDDEN: " + newPlayer.PlayerClient.ClientName
            /// "GAME START"


            //Nem kell, mert nincs naplózás:
            //if (msg == "GAME START")
            //    Init.Inicializal();

        }           //az elején, 10 secenként és a végén
    }
}
