﻿using System.Collections.Generic;
using System.Linq;

namespace Teszteles01
{
    static class ScoutMap
    {
        //public static bool ScoutsOnRight { get; set; }
        //public static bool ScoutsOut { get; set; }

        public static int ScoutsNeeded { get; set; }
        public static int ActualNumberOfScouts { get { return ScoutList.Count; } }

        //static OwnShip[,] elements;

        ///// <summary>
        ///// GET A mapon lévő felderítők, főleg a szöveges fájlba való kiírása használatos
        ///// </summary>
        //public static OwnShip[,] Elements
        //{
        //    get { return elements; }
        //}

        public static List<OwnShip> ScoutList { get; private set; }

        ///// <summary>
        ///// Megadja, hogy minden scout egy helyben áll-e
        ///// </summary>
        //public static bool AllScoutsStanding
        //{
        //    get
        //    {
        //        return ScoutList.Count(x => x.DestinationX > -1 && x.DestinationY > -1) == 0;
        //    }
        //}

        public static void Initialize(List<OwnShip> scouts, int size_x, int size_y)
        {           
            //Belső lista feltöltése
            ScoutList = scouts;

            ////Inicializálás:
            //elements = new OwnShip[size_x, size_y];
            //for (int i = 0; i < elements.GetLength(0); i++)
            //    for (int j = 0; j < elements.GetLength(1); j++)
            //        elements[i, j] = new OwnShip();

            ////2D tömb feltöltése
            //for (int i = 0; i < scouts.Count; i++)
            //{
            //    OwnShip scout = scouts[i];
            //    elements[(int)scout.PosX, (int)scout.PosY] = scout;
            //}
        }

        ///// <summary>
        ///// Kiírja egy fájlba a ScoutMap aktuális tartalmát.
        ///// </summary>
        ///// <param name="fileName"></param>
        //public static void FajlbaIr(string fileName)
        //{
        //    using (System.IO.StreamWriter sw = new System.IO.StreamWriter(fileName))
        //    {
        //        for (int i = 0; i < elements.GetLength(0); i++)
        //        {
        //            for (int j = 0; j < elements.GetLength(1); j++)
        //            {
        //                //A mapot y=-x egyenes mentén tükrözzük, mert a felületen is így látszik.
        //                if (elements[j, i].NumberOfUnits == 1)
        //                    sw.Write("o");
        //                else
        //                    sw.Write("-");
        //            }
        //            sw.WriteLine();
        //        }
        //    }
        //}
    }
}