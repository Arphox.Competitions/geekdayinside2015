﻿using SpaceBattle.Common;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Teszteles01
{
    static class AI1
    {
        public static class Adatfeldolgozas
        {
            public static void Feldolgozas(List<GameItemDescriptor> gameItems)
            {
                Feldolgozo feldolgozo = new Feldolgozo(gameItems);

                OwnPlanets = feldolgozo.OwnPlanets;
                OwnShips = feldolgozo.OwnShips;
                EnemyPlanets = feldolgozo.EnemyPlanets;
                EnemyShips = feldolgozo.EnemyShips;
                NeutralPlanets = feldolgozo.NeutralPlanets;

                PlanetMapGeneralas();
                ScoutMapGeneralas();
            }
            static void PlanetMapGeneralas()
            {
                if (!PlanetMap.Initialized)
                    PlanetMap.Initialize(OwnPlanets, EnemyPlanets, NeutralPlanets);
                else
                {
                    PlanetMap.Refresh(OwnPlanets, EnemyPlanets, NeutralPlanets);
                    //PlanetMap.FajlbaIr(Settings.FilePaths.PlanetMapTXT);
                }
            }
            static void ScoutMapGeneralas()
            {
                ScoutMap.Initialize(FelderitokDetektalasa(), Settings.MapSizeX, Settings.MapSizeY);
                //ScoutMap.FajlbaIr(Settings.FilePaths.ScoutMapTXT);

                ScoutMap.ScoutsNeeded = (Settings.MapSizeX / ShipLatotav) + (Settings.MapSizeX % ShipLatotav == 0 ? 0 : 1);
            }
        }
        static class Felderites
        {
            class OrszemPar
            {
                public int PlanetID { get; private set; }
                public int ShipID { get; private set; }
                public OrszemPar(int planetID, int shipID)
                {
                    PlanetID = planetID;
                    ShipID = shipID;
                }
                public OwnShip GetShip()
                {
                    foreach (OwnShip item in OwnShips)
                    {
                        if (item.ItemId == ShipID)
                            return item;
                    }
                    return null;
                }
                public GameItem GetPlanet()
                {
                    var q = from x in PlanetMap.PermaNeutralPlanets
                            where x.ItemId == PlanetID
                            select x;
                    if (q.Count() != 0)
                    {
                        return q.ElementAt(0);
                    }
                    else
                    {
                        foreach (EnemyPlanet item in EnemyPlanets)
                        {
                            if (item.ItemId == PlanetID)
                            {
                                return item;
                            }
                        }
                        //foreach (OwnPlanet item in OwnPlanets)
                        //{
                        //    if (item.ItemId == PlanetID)
                        //    {
                        //        return item;
                        //    }
                        //}
                        return null;
                    }
                }

                public EnemyPlanet GetEnemyPlanet()
                {
                    var q = from x in EnemyPlanets
                            where x.ItemId == PlanetID
                            select x;
                    if (q.Count() != 0)
                    {
                        return q.ElementAt(0);
                    }
                    else
                    {
                        return null;
                    }
                }
                public OwnPlanet GetOwnPlanet()
                {
                    var q = from x in OwnPlanets
                            where x.ItemId == PlanetID
                            select x;
                    if (q.Count() != 0)
                    {
                        return q.ElementAt(0);
                    }
                    else
                    {
                        return null;
                    }
                }
                public bool Tamadhato()
                {
                    OwnShip Ship = GetShip();
                    GameItem Planet = GetPlanet();
                    if (Ship != null && Planet != null && Planet.PosX == Ship.PosX && Planet.PosY == Ship.PosY)
                    {
                        if (EnemyPlanets.Contains(Planet))
                        {
                            return GetEnemyPlanet().NumberOfUnits <= Ship.NumberOfUnits;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            public static List<BattleCommand> Felderit()
            {
                if (!Traffipax.Vege)
                {
                    if (statuses.Count == 0)
                    {
                        statuses.Add(0);
                        resetters.Add(0);
                        iterators.Add(1);
                    }
                    return Traffipax.Indit();
                }
                else if (Settings.Behaviour.FELDERIT)
                {
                    return FelderitesPaszta();
                    //return FelderitesRandom();
                    //return FelderitesFesu();
                    //return new List<BattleCommand>();
                }
                else
                    return new List<BattleCommand>();
            }
            //static List<BattleCommand> FelderitesRandom()
            //{
            //    const bool FOLYAMATOS_UTANPOTLAS = true;
            //    const int KEZDETI_FELDERITOK_SZAMA = 5;
            //    const int MAX_FELDERITOK_SZAMA = 5;
            //    const int KULDESI_IDOKOZ = 20; //minél nagyobb, annál lassabb, de legalább 2

            //    List<BattleCommand> parancsok = new List<BattleCommand>();
            //    List<OwnShip> felderitok = FelderitokDetektalasa();

            //    if (felderitok.Count() < KEZDETI_FELDERITOK_SZAMA - 1)
            //    {   //ha még nincs felderítőnk, akkor a bolygónkról válasszunk le egyet
            //        OwnPlanet planet = OwnPlanets.Where(x => x.NumberOfUnits == OwnPlanets.Max(y => y.NumberOfUnits)).First();
            //        parancsok.Add(Commands.Split(planet.ItemId, 1));
            //    }
            //    else if (Orajel % KULDESI_IDOKOZ == 0)
            //    {
            //        foreach (OwnShip felderito in felderitok)
            //        {
            //            parancsok.Add(Commands.Move(felderito.ItemId, random.Next(0, 40), random.Next(0, 40)));
            //        }
            //    }

            //    if (FOLYAMATOS_UTANPOTLAS && felderitok.Count < MAX_FELDERITOK_SZAMA)
            //    {
            //        if (Orajel % (KULDESI_IDOKOZ - 1) == 0)
            //        {
            //            OwnPlanet planet = OwnPlanets.Where(x => x.NumberOfUnits == OwnPlanets.Max(y => y.NumberOfUnits)).First();
            //            parancsok.Add(Commands.Split(planet.ItemId, 1));
            //        }
            //    }

            //    return parancsok;
            //}
            //static List<BattleCommand> FelderitesFesu()
            //{
            //    List<BattleCommand> parancsok = new List<BattleCommand>();

            //    //Scoutok létrehozása:
            //    if (ScoutMap.ActualNumberOfScouts < ScoutMap.ScoutsNeeded)
            //    {
            //        //Annyit választunk le, amennyi még kell ahhoz, hogy elég Felderítőnk legyen
            //        int mennyiScoutKellMeg = ScoutMap.ScoutsNeeded - ScoutMap.ActualNumberOfScouts;
            //        for (int i = 0; i < mennyiScoutKellMeg; i++)
            //        {
            //            OwnPlanet planet = OwnPlanets.Where(x => x.NumberOfUnits == OwnPlanets.Max(y => y.NumberOfUnits)).First();
            //            parancsok.Add(Commands.Split(planet.ItemId, 1));
            //        }
            //    }
            //    //Helyre küldés (Ha még nincsenek kiküldve és pont annyi scout van amennyit ki akarunk küldeni)
            //    if (!ScoutMap.ScoutsOut && ScoutMap.ActualNumberOfScouts == ScoutMap.ScoutsNeeded)
            //    {
            //        for (int i = 0; i < ScoutMap.ActualNumberOfScouts; i++)
            //        {
            //            parancsok.Add(Commands.Move(ScoutMap.ScoutList[i].ItemId, 0, i * ShipLatotav * 2 + ShipLatotav / 2));
            //        }
            //        ScoutMap.ScoutsOut = true;
            //    }
            //    //Mozgatás:
            //    if (ScoutMap.ScoutsOut && ScoutMap.AllScoutsStanding)
            //    {
            //        if (ScoutMap.ScoutsOnRight) //Ha jobb oldalon vannak a scoutok
            //        {
            //            for (int i = 0; i < ScoutMap.ActualNumberOfScouts; i++)
            //            {
            //                parancsok.Add(Commands.Move(ScoutMap.ScoutList[i].ItemId, 0, i * ShipLatotav * 2 + ShipLatotav / 2));
            //            }
            //            ScoutMap.ScoutsOnRight = !ScoutMap.ScoutsOnRight;
            //        }
            //        else
            //        {
            //            for (int i = 0; i < ScoutMap.ActualNumberOfScouts; i++)
            //            {
            //                parancsok.Add(Commands.Move(ScoutMap.ScoutList[i].ItemId, Settings.MapSizeX - 1, i * ShipLatotav * 2 + ShipLatotav / 2));
            //            }
            //            ScoutMap.ScoutsOnRight = !ScoutMap.ScoutsOnRight;
            //        }
            //    }

            //    return parancsok;
            //}
            #region Pásztázás
            static List<int> statuses = new List<int>();
            static List<int> resetters = new List<int>();
            static List<int> iterators = new List<int>();
            //static int status = 0;
            //static int iterator = 1;
            //static int resetter = 0;
            static int counter = 0;           
            static List<BattleCommand> FelderitesPaszta()
            {
                List<BattleCommand> parancsok = new List<BattleCommand>();
                counter++;
                if ((ScoutMap.ScoutList.Count < OwnPlanets.OsszesNoU() * Settings.MaxScoutSzazalek / 100 && ScoutMap.ScoutList.Count < Settings.MapSizeY)
                    || ScoutMap.ScoutList.Count == 0)
                {
                    OwnPlanet planet = OwnPlanets.Where(x => x.NumberOfUnits == OwnPlanets.Max(y => y.NumberOfUnits)).First();
                    parancsok.Add(Commands.Split(planet.ItemId, 1));
                    statuses.Add(0);
                    resetters.Add(0);
                    iterators.Add(1);
                    counter = 0;
                }
                if (ScoutMap.ActualNumberOfScouts != 0)
                {
                    for (int i = 0; i < ScoutMap.ScoutList.Count; i++)
                    {
                        Pasztazo(parancsok, i);
                    }
                }
                return parancsok;
            }
            static void Pasztazo(List<BattleCommand> parancsok, int hanyadik)
            {
                OwnShip scout = ScoutMap.ScoutList[hanyadik];

                if (iterators[hanyadik] <= (Settings.MapSizeY / ScoutMap.ScoutList.Count))
                {
                    if (statuses[hanyadik] < 1 && scout.DestinationX == -1 && scout.DestinationY == -1)
                    {
                        parancsok.Add(Commands.Move(scout.ItemId, 1, 1 + hanyadik * Settings.MapSizeY / ScoutMap.ScoutList.Count));
                        statuses[hanyadik]++;
                    }
                    else if (statuses[hanyadik] < 2 && scout.DestinationX == -1 && scout.DestinationY == -1)
                    {
                        parancsok.Add(Commands.Move(scout.ItemId, Settings.MapSizeX - 1, scout.PosY));
                        statuses[hanyadik]++;
                    }
                    else if (statuses[hanyadik] < 3 && scout.DestinationX == -1 && scout.DestinationY == -1)
                    {
                        parancsok.Add(Commands.Move(scout.ItemId, Settings.MapSizeX - 1, scout.PosY + 2));
                        statuses[hanyadik]++;
                    }
                    else if (statuses[hanyadik] < 4 && scout.DestinationX == -1 && scout.DestinationY == -1)
                    {
                        parancsok.Add(Commands.Move(scout.ItemId, 1, scout.PosY));
                        statuses[hanyadik]++;
                    }
                    else if (statuses[hanyadik] < 5 && scout.DestinationX == -1 && scout.DestinationY == -1)
                    {
                        parancsok.Add(Commands.Move(scout.ItemId, 1, scout.PosY + 2));
                        statuses[hanyadik]++;
                    }
                    else if (scout.DestinationX == -1 && scout.DestinationY == -1)
                    {
                        statuses[hanyadik] = 1;
                        iterators[hanyadik] += 2;
                    }                   
                }
                else
                {
                    if (scout.DestinationX == -1 && scout.DestinationY == -1)
                    {
                        parancsok.Add(Commands.Move(scout.ItemId, 1, 1 + hanyadik * Settings.MapSizeY / ScoutMap.ScoutList.Count));
                        iterators[hanyadik] = 1;
                    }
                }
            }
            #endregion
            #region Őrszemezés
            static List<OwnShip> Orszemek = new List<OwnShip>();
            static List<OrszemPar> OrszemParok = new List<OrszemPar>();
            static List<int> KikuldottOrszemek = new List<int>();
            static List<int> KikuldottTransferShipek = new List<int>();
            static bool OrszemVanRajta(float x, float y)
            {
                foreach (OwnShip item in Orszemek)
                {
                    if (item.PosX == x && item.PosY == y || item.DestinationX == x && item.DestinationY == y)
                        return true;
                }
                return false;
            }
            public static List<BattleCommand> OrszemFigyel()
            {
                TransferShip.Takaritas(transferShips);
                List<BattleCommand> parancsok = new List<BattleCommand>();
                OwnPlanet home = (from x in OwnPlanets
                                  where x.NumberOfUnits == OwnPlanets.Min(y => y.NumberOfUnits)
                                  select x).First();
                foreach (OrszemPar item in OrszemParok)
                {
                    OwnShip Ship = item.GetShip();
                    GameItem Planet = item.GetPlanet();
                    if (item.Tamadhato())
                    {
                        parancsok.Add(Commands.Shoot(Ship.ItemId, Ship.NumberOfUnits, Planet.ItemId));
                    }
                    else if (EnemyPlanets.Contains(Planet))
                    {
                        EnemyPlanet ePlanet = item.GetEnemyPlanet();
                        if (Ship != null && ePlanet.NumberOfUnits > Ship.NumberOfUnits)
                        {
                            TransferShip t = new TransferShip(Ship, home);
                            transferShips.Add(t);
                            KikuldottTransferShipek.Add(t.Ship.ItemId);
                        }
                    }
                    else if (OwnPlanets.Contains(Planet))
                    {
                        OwnPlanet oPlanet = item.GetOwnPlanet();
                        if (Ship != null && oPlanet.NumberOfUnits > Ship.NumberOfUnits)
                        {
                            TransferShip t = new TransferShip(Ship, home);
                            transferShips.Add(t);
                            KikuldottTransferShipek.Add(t.Ship.ItemId);
                        }
                    }
                }
                for (int i = 0; i < transferShips.Count; i++)
                {
                    parancsok.Add(transferShips[i].Activate(OwnShips));
                }
                return parancsok;
            }
            public static List<BattleCommand> Orszemkihelyez()
            {
                Orszemek = OrszemekDetektalasa();
                List<BattleCommand> parancsok = new List<BattleCommand>();
                if (Orszemek.Count < PlanetMap.PermaNeutralPlanets.Count)
                {
                    OwnPlanet legnagyobb = OwnPlanets.Legnagyobb();
                    if (legnagyobb.NumberOfUnits * Settings.MaxOrszemSzazalek / 100 > 2)
                    {
                        parancsok.Add(Commands.Split(legnagyobb.ItemId, 2));
                    }
                }
                for (int i = 0; i < Orszemek.Count; i++)
                {
                    if (!KikuldottOrszemek.Contains(Orszemek[i].ItemId) && Orszemek[i].DestinationX == -1 && Orszemek[i].DestinationY == -1)
                    {
                        int j = 0;
                        while (j < PlanetMap.PermaNeutralPlanets.Count && OrszemVanRajta(PlanetMap.PermaNeutralPlanets[j].PosX, PlanetMap.PermaNeutralPlanets[j].PosY))
                        {
                            j++;
                        }
                        if (j < PlanetMap.PermaNeutralPlanets.Count)
                        {
                            parancsok.Add(Commands.Move(Orszemek[i].ItemId, PlanetMap.PermaNeutralPlanets[j].PosX, PlanetMap.PermaNeutralPlanets[j].PosY));
                            OrszemParok.Add(new OrszemPar(PlanetMap.PermaNeutralPlanets[j].ItemId, Orszemek[i].ItemId));
                            KikuldottOrszemek.Add(Orszemek[i].ItemId);
                        }
                    }
                }
                return parancsok;
            }
            static List<OwnShip> OrszemekDetektalasa()
            {
                return (from x in OwnShips
                        where x.NumberOfUnits == 2
                        select x).ToList();
            }
            #endregion
        }
        static class Tamadas
        {
            static List<PointF> IdeTamadtunkMar = new List<PointF>();

            public static List<BattleCommand> Tamad()
            {
                List<BattleCommand> parancsok = new List<BattleCommand>();
                FarShot.Takaritas(farShots);//FarShot takarítás

                if (Traffipax.Vege)
                {                    
                    Strat2();
                    if (Settings.VegJatek)
                    {
                        Neutralozas();
                    }
                }

                #region FarShotok aktiválása
                for (int i = 0; i < farShots.Count; i++)
                {
                    parancsok.Add(farShots[i].Activate(OwnShips));
                }
                #endregion
                return parancsok;
            }

            #region Támadási stratégiák
            public static void Neutralozas()
            {
                if (PlanetMap.PermaNeutralPlanets.Count >= 1)
                {
                    NeutralPlanet cel = PlanetMap.PermaNeutralPlanets.First();
                    OwnPlanet forras = OwnPlanets.LegkozelebbiAmiNagyobbMint(cel, cel.NumberOfUnits);
                    if (forras != null
                        && !IdeTamadtunkMar.Contains(new PointF(cel.PosX, cel.PosY))   //ha még nem támadtunk ide
                        && (forras.NumberOfUnits > cel.NumberOfUnits + 1))          //és tudunk
                    {
                        FarShot fs = new FarShot(forras, cel, cel.NumberOfUnits + 1);
                        farShots.Add(fs);
                        IdeTamadtunkMar.Add(new PointF(cel.PosX, cel.PosY));
                    }
                }
            }
            //static void Strat1()
            //{
            //    if (Orajel % 50 == 0)
            //        IdeTamadtunkMar = new List<PointF>();

            //    for (int i = 0; i < EnemyPlanets.Count; i++)
            //    {
            //        EnemyPlanet ep = EnemyPlanets[i];
            //        List<OwnPlanet> legkozelebbiOwnPlanetek = OwnPlanets.OrderByDescending(y => Matek.KetItemTavolsaga(y, ep)).ToList();

            //        //Keresi a legközelebbi bolygót, amelyről elküldve el tudja foglalni:
            //        for (int k = 0; k < legkozelebbiOwnPlanetek.Count; k++)
            //        {
            //            double distance = Matek.KetItemTavolsaga(ep, legkozelebbiOwnPlanetek[i]);
            //            double speed = Settings.SPEED;
            //            double time = distance / speed;
            //            //int darab = enemyPlanet.NumberOfUnits + (int)(ido * enemyPlanet.IncTime) + (int)(1 / enemyPlanet.IncTime);
            //            int darab = ep.NumberOfUnits + (int)(time * ep.IncTime) + (int)(ep.IncTime / 30 * 500);

            //            if (legkozelebbiOwnPlanetek[i].NumberOfUnits > darab
            //                && !IdeTamadtunkMar.Contains(new PointF(ep.PosX, ep.PosY)))
            //            {
            //                FarShot fs = new FarShot(legkozelebbiOwnPlanetek[i], ep, darab);
            //                farShots.Add(fs);
            //                IdeTamadtunkMar.Add(new PointF(ep.PosX, ep.PosY));
            //                break;
            //            }
            //        }
            //    }
            //}
            static void Strat2() //Képes összevont támadásokra is
            {
                if (Orajel % 100 == 0)
                    IdeTamadtunkMar = new List<PointF>();

                //Végigmegy minden EnemyPlanet-en, mindegyikre próbál támadni.
                for (int a = 0; a < EnemyPlanets.Count; a++)
                {
                    EnemyPlanet enemyPlanet = EnemyPlanets[a];
                    bool attackSent = false;

                    //Ha a közelmúltban nem támadtunk még ide (értsd előző x órajelben), akkor
                    if (!IdeTamadtunkMar.Contains(new PointF(enemyPlanet.PosX, enemyPlanet.PosY)))
                    {
                        //Veszi az adott ellenfélhez legközelebbi OwnPlanetek távolság szerint növekvően rendezett listáját
                        List<OwnPlanet> legkozelebbiOwnPlanetek = OwnPlanets.OrderBy(y => Matek.KetItemTavolsaga(y, enemyPlanet)).ToList();

                        #region Egy helyről támadás
                        //Keresi a legközelebbi bolygót, amelyről elküldve el tudja foglalni, először a legközelebbitől
                        for (int b = 0; b < legkozelebbiOwnPlanetek.Count; b++)
                        {
                            OwnPlanet legkozelebbiOwnPlanet = legkozelebbiOwnPlanetek[b];

                            int darab = Matek.MennyivelKellTamadni(enemyPlanet, legkozelebbiOwnPlanet);
                            if (darab <= 2) darab+=2;
                            //Ha arról a bolygóról tud elegendő mennyiséggel támadni, akkor támad
                            if (legkozelebbiOwnPlanet.NumberOfUnits > darab)
                            {
                                FarShot fs = new FarShot(legkozelebbiOwnPlanet, enemyPlanet, darab);
                                farShots.Add(fs);
                                IdeTamadtunkMar.Add(new PointF(enemyPlanet.PosX, enemyPlanet.PosY));
                                attackSent = true;
                                break; //ha kiküldte a támadást, nem keres tovább
                            }
                        }
                        #endregion

                        #region Összevont támadás
                        if (!attackSent) //Ha még nem küldtünk el oda egy helyről támadást, akkor próbálkozunk összevontan
                        {
                            bool osszevontanTamadtunk = false;
                            //Összesen nem tudjuk, hogy mennyit kell küldeni, mert függ az időtől, de ha a legtávolabbi bolygó
                            //számoljuk az odaérési "időt" akkor nem lőhetünk mellé.
                            //=====================================================
                            //A legtávolabbi bolygótól számítjuk a távolságot:
                            int ellenfelBolygoEro = Matek.MennyivelKellTamadni(enemyPlanet, legkozelebbiOwnPlanetek[legkozelebbiOwnPlanetek.Count - 1]);
                            //Csak akkor támadunk, ha az összevont támadás ereje max. a mi teljes erőnk felét veszi igénybe
                            if (!Settings.VegJatek)
                            {
                                if (OwnPlanets.OsszesNoU() > 2 * ellenfelBolygoEro)
                                {
                                    int megEnergia = ellenfelBolygoEro; //A támadáshoz még szükséges energia
                                    for (int c = 0; c < legkozelebbiOwnPlanetek.Count; c++)
                                    {
                                        OwnPlanet legkozelebbiOwnPlanet = legkozelebbiOwnPlanetek[c];

                                        //Ha több energia kell még a támadáshoz, mint amennyi az adott bolygónak vanm
                                        if (megEnergia >= legkozelebbiOwnPlanet.NumberOfUnits)
                                        {
                                            //Akkor onnan elküldünk, amennyit lehet
                                            int kuldeni = legkozelebbiOwnPlanet.NumberOfUnits - 1;
                                            if (kuldeni <= 2) kuldeni += 2;
                                            FarShot fs = new FarShot(legkozelebbiOwnPlanet, enemyPlanet, kuldeni);
                                            farShots.Add(fs);
                                            megEnergia -= legkozelebbiOwnPlanet.NumberOfUnits - 1;
                                        }
                                        else //De ha kevesebb, akkor nem kell az összeset elküldeni a bolygóról
                                        {   //Akkor nem kell többet keresnünk, ennyi bolygóról elég támadni:
                                            int kuldeni = legkozelebbiOwnPlanet.NumberOfUnits - megEnergia + 1;
                                            if (kuldeni <= 2) kuldeni += 2;
                                            FarShot fs = new FarShot(legkozelebbiOwnPlanet, enemyPlanet, kuldeni);
                                            farShots.Add(fs);
                                            osszevontanTamadtunk = true;
                                            break;
                                        }
                                    }
                                    if (osszevontanTamadtunk)
                                        IdeTamadtunkMar.Add(new PointF(enemyPlanet.PosX, enemyPlanet.PosY));
                                }
                            }
                            else
                            {
                                int megEnergia = ellenfelBolygoEro; //A támadáshoz még szükséges energia
                                for (int c = 0; c < legkozelebbiOwnPlanetek.Count; c++)
                                {
                                    OwnPlanet legkozelebbiOwnPlanet = legkozelebbiOwnPlanetek[c];

                                    //Ha több energia kell még a támadáshoz, mint amennyi az adott bolygónak vanm
                                    if (megEnergia >= legkozelebbiOwnPlanet.NumberOfUnits)
                                    {
                                        //Akkor onnan elküldünk, amennyit lehet
                                        int kuldeni = legkozelebbiOwnPlanet.NumberOfUnits - 1;
                                        if (kuldeni <= 2) kuldeni += 2;
                                        FarShot fs = new FarShot(legkozelebbiOwnPlanet, enemyPlanet, kuldeni);
                                        farShots.Add(fs);
                                        megEnergia -= legkozelebbiOwnPlanet.NumberOfUnits - 1;
                                    }
                                    else //De ha kevesebb, akkor nem kell az összeset elküldeni a bolygóról
                                    {   //Akkor nem kell többet keresnünk, ennyi bolygóról elég támadni:
                                        int kuldeni = legkozelebbiOwnPlanet.NumberOfUnits - megEnergia + 1;
                                        if (kuldeni <= 2) kuldeni += 2;
                                        FarShot fs = new FarShot(legkozelebbiOwnPlanet, enemyPlanet, kuldeni);
                                        farShots.Add(fs);
                                        osszevontanTamadtunk = true;
                                        break;
                                    }
                                }
                                if (osszevontanTamadtunk)
                                    IdeTamadtunkMar.Add(new PointF(enemyPlanet.PosX, enemyPlanet.PosY));
                            }
                        }
                        #endregion
                    }
                }
            }
            #endregion
        }
        //static class Transzfer
        //{
        //    public static List<BattleCommand> TranszferProba()
        //    {
        //        List<BattleCommand> parancsok = new List<BattleCommand>();
        //        TransferShot.Takaritas(transferShots);

        //        if (OwnPlanets.Count > 1)
        //        {
        //            OwnPlanet forras = OwnPlanets[0];
        //            OwnPlanet cel = OwnPlanets[1];
        //            int nou = 5;
        //            TransferShot ts = new TransferShot(forras, cel, nou);

        //            if (forras.NumberOfUnits > 5 && !TransferShot.VanEMarIlyenTransferShot(transferShots, ts))
        //            {
        //                transferShots.Add(ts);
        //            }
        //        }

        //        //Aktiválás:
        //        for (int i = 0; i < transferShots.Count; i++)
        //            parancsok.Add(transferShots[i].Activate(OwnShips));

        //        return parancsok;
        //    }
        //}
        //static class Faker
        //{
        //    static bool elegendoKikuldve = false;
        //    static int FakerMaxNumber = 5;
        //    static int FakerNoU = 2;

        //    public static List<BattleCommand> FakerProba()
        //    {
        //        if (Orajel % 10 == 0 && OwnShips.Count(x => x.NumberOfUnits == FakerNoU) < FakerMaxNumber)
        //            elegendoKikuldve = false;

        //        List<BattleCommand> parancsok = new List<BattleCommand>();
        //        FakeShot.Takaritas(fakeShots);

        //        if (!elegendoKikuldve && OwnPlanets.First().NumberOfUnits > FakerNoU)
        //        {
        //            bool neutralraKikuldve = false;
        //            if (NeutralPlanets.Count > 0)
        //            {
        //                FakeShot fs = new FakeShot(OwnPlanets.First(), NeutralPlanets[random.Next(0, NeutralPlanets.Count)], FakerNoU);
        //                fakeShots.Add(fs);
        //                elegendoKikuldve = true; neutralraKikuldve = true;
        //            }
        //            if (EnemyPlanets.Count > 0 && !neutralraKikuldve)
        //            {
        //                FakeShot fs = new FakeShot(OwnPlanets.First(), EnemyPlanets[random.Next(0, EnemyPlanets.Count)], FakerNoU);
        //                fakeShots.Add(fs);
        //                elegendoKikuldve = true;
        //            }
        //        }

        //        //Aktiválás
        //        for (int i = 0; i < fakeShots.Count; i++)
        //        {
        //            try
        //            { parancsok.Add(fakeShots[i].GetNextCommand(OwnShips)); }
        //            catch (NemEgyertelmuHajo e)
        //            {
        //                //Ha nem egyértelmű a választás, akkor hívjunk mindenkit vissza
        //                List<OwnShip> szurt = OwnShips.Where(x => x.NumberOfUnits == e.NumberOfUnits).ToList();
        //                foreach (OwnShip item in szurt)
        //                {
        //                    int otherItemID = OwnPlanets[random.Next(0, OwnPlanets.Count)].ItemId;
        //                    parancsok.Add(Commands.Shoot(item.ItemId, item.NumberOfUnits, otherItemID));
        //                }
        //            }
        //        }
        //        return parancsok;
        //    }
        //}

        public static int Orajel { get; set; }
        static int ShipLatotav = 2;
        static Random random = new Random();
        static EnemyPlanet lastResortTarget;
        static List<FarShot> farShots = new List<FarShot>();
        static List<TransferShip> transferShips = new List<TransferShip>();

        #region Actual Item Lists

        public static List<OwnPlanet> OwnPlanets = new List<OwnPlanet>();
        public static List<OwnShip> OwnShips = new List<OwnShip>();
        public static List<EnemyPlanet> EnemyPlanets = new List<EnemyPlanet>();
        public static List<EnemyShip> EnemyShips = new List<EnemyShip>();
        public static List<NeutralPlanet> NeutralPlanets = new List<NeutralPlanet>();

        #endregion
        static EnemyPlanet GetEnemyPlanet(int id)
        {
            var q = from x in PlanetMap.PermaEnemyPlanets
                    where x.ItemId == id
                    select x;
            if (q.Count() != 0)
            {
                return q.ElementAt(0);
            }
            else
            {
                return null;
            }
        }
        static OwnPlanet GetOwnPlanet(int id)
        {
         var q = from x in OwnPlanets
                    where x.ItemId == id
                    select x;
            if (q.Count() != 0)
            {
                return q.ElementAt(0);
            }
            else
            {
                return null;
            }
        }
        static OwnPlanet GetOwnPlanet(float x, float y)
        {
            foreach (OwnPlanet item in OwnPlanets)
            {
                if (item.PosX == x && item.PosY == y)
                {
                    return item;
                }
            }
            return null;
        }
        private static List<BattleCommand> LastResort()
        {
            int id = -1;
            List<BattleCommand> parancsok = new List<BattleCommand>();
            if (PlanetMap.PermaEnemyPlanets.Count != 0)
            {
                var q = (from x in PlanetMap.PermaEnemyPlanets
                          where x.NumberOfUnits == PlanetMap.PermaEnemyPlanets.Min(y => y.NumberOfUnits) && GetOwnPlanet(x.ItemId) == null
                          select x);
                if (q.Count() != 0)
                {
                    id = q.ElementAt(0).ItemId;
                }
            }
            var q2 = from x in PlanetMap.PermaEnemyPlanets
                    where x.ItemId == id
                    select x;
            if (q2.Count() == 0)
            {
                return new List<BattleCommand>();
            }
            else
            {
                if (lastResortTarget == null)
                {
                    lastResortTarget = GetEnemyPlanet(id);
                }
                if (GetOwnPlanet(lastResortTarget.ItemId) != null)
                {
                    lastResortTarget = null;
                }
                if (lastResortTarget != null)
                {
                    foreach (OwnShip item in OwnShips)
                    {
                        if (item.PosX != lastResortTarget.PosX || item.PosY != lastResortTarget.PosY)
                        {
                            parancsok.Add(Commands.Move(item.ItemId, lastResortTarget.PosX, lastResortTarget.PosY));
                        }
                        else
                        {
                            parancsok.Add(Commands.Shoot(item.ItemId, item.NumberOfUnits, lastResortTarget.ItemId));
                        }
                    }
                }
            }
            foreach (OwnShip item in OwnShips)
            {
                OwnPlanet o = GetOwnPlanet(item.PosX, item.PosY);
                if (item.DestinationX == -1 && item.DestinationY == -1 && o != null)
                {
                    parancsok.Add(Commands.Shoot(item.ItemId, item.NumberOfUnits, o.ItemId));
                }
            }
            return parancsok;
        }
        public static List<BattleCommand> KiadottParancsok()
        {
            Settings.VegJatek = Settings.RemainingTime < 11;
            List<BattleCommand> parancsok = new List<BattleCommand>();

            if (OwnPlanets.Count == 0)
            {
                return LastResort();
            }
            if (Settings.RemainingTime <= 10)
            {
                parancsok = parancsok.Concat(Felderites.Felderit().Concat(
                                                LastResort()).Concat(
                                                    Tamadas.Tamad())).ToList();
            }
            else
            {
                parancsok = parancsok.Concat(Felderites.Felderit()).Concat(Felderites.Orszemkihelyez()).Concat(Felderites.OrszemFigyel()).ToList(); //SOSE KOMMENTELD KI!
            }
            if (Settings.Behaviour.TAMAD)
                parancsok = parancsok.Concat(Tamadas.Tamad()).ToList();

            return parancsok;
        }

        /// <summary>
        /// Megadja a kiküldött felderítőinket. Felderítő = 1 energiaszintű saját hajó
        /// </summary>
        /// <returns></returns>
        static List<OwnShip> FelderitokDetektalasa()
        {
            List<OwnShip> lista = new List<OwnShip>();

            lista = (from x in OwnShips
                     where x.NumberOfUnits == 1
                     select x).ToList();

            return lista;
        }
    }
}