﻿using System.Collections.Generic;
using System.Linq;

namespace Teszteles01
{
    static class PlanetMap
    {
        public static bool Initialized { get; private set; }

        //static GameItem[,] elements = new GameItem[Settings.MapSizeX, Settings.MapSizeY];
        ///// <summary>
        ///// A mapon lévő bolygók.
        ///// </summary>
        //public static GameItem[,] Elements
        //{
        //    get { return elements; }
        //}

        //Constructor
        public static void Initialize(List<OwnPlanet> OwnPlanets, List<EnemyPlanet> EnemyPlanets, List<NeutralPlanet> NeutralPlanets)
        {
            PermaNeutralPlanets = NeutralPlanets;
            PermaEnemyPlanets = EnemyPlanets;

            ////items beillesztése
            //for (int i = 0; i < OwnPlanets.Count; i++)
            //{
            //    OwnPlanet item = OwnPlanets[i];
            //    elements[(int)item.PosX, (int)item.PosY] = item;
            //}

            ////EnemyPlanets beillesztése
            //for (int i = 0; i < EnemyPlanets.Count; i++)
            //{
            //    EnemyPlanet item = EnemyPlanets[i];
            //    elements[(int)item.PosX, (int)item.PosY] = item;
            //}

            ////NeutralPlanetsList beillesztése
            //for (int i = 0; i < NeutralPlanets.Count; i++)
            //{
            //    NeutralPlanet item = NeutralPlanets[i];
            //    elements[(int)item.PosX, (int)item.PosY] = item;
            //}

            Initialized = true;
        }
        public static List<NeutralPlanet> PermaNeutralPlanets { get; private set; }
        public static List<EnemyPlanet> PermaEnemyPlanets { get; private set; }

        /// <summary>
        /// Aktualizálja a planetMapot.
        /// </summary>
        /// <param name="newMap"></param>
        public static void Refresh(List<OwnPlanet> OwnPlanets, List<EnemyPlanet> EnemyPlanets, List<NeutralPlanet> NeutralPlanets)
        {
            //PermaNeutralPlanetList beillesztése
            for (int i = 0; i < NeutralPlanets.Count; i++)
            {
                int indexof = PermaNeutralPlanets.IndexOf(NeutralPlanets[i]);
                if (indexof > -1)
                {
                    PermaNeutralPlanets[indexof] = NeutralPlanets[i];
                }
                else
                {
                    PermaNeutralPlanets.Add(NeutralPlanets[i]);
                }
            }
            for (int i = PermaNeutralPlanets.Count - 1; i >= 0 ; i--)
			{
                if (EnemyPlanets.Where(x => x.ItemId == PermaNeutralPlanets[i].ItemId).Count() == 1)
                {
                    PermaNeutralPlanets.RemoveAt(i);
                }
            }

            //PermaEnemyPlanetList beillesztése
            for (int i = 0; i < EnemyPlanets.Count; i++)
            {
                int indexof = PermaEnemyPlanets.IndexOf(EnemyPlanets[i]);
                if (indexof > -1)
                {
                    PermaEnemyPlanets[indexof] = EnemyPlanets[i];
                }
                else
                {
                    PermaEnemyPlanets.Add(EnemyPlanets[i]);
                }
            }
            for (int i = PermaEnemyPlanets.Count - 1; i >= 0; i--)
            {
                if (OwnPlanets.Where(x => x.ItemId == PermaEnemyPlanets[i].ItemId).Count() == 1
                    || NeutralPlanets.Where(x => x.ItemId == PermaEnemyPlanets[i].ItemId).Count() == 1)
                {
                    PermaEnemyPlanets.RemoveAt(i);
                }
            }

            ////items beillesztése
            //for (int i = 0; i < OwnPlanets.Count; i++)
            //{
            //    OwnPlanet item = OwnPlanets[i];
            //    elements[(int)item.PosX, (int)item.PosY] = item;
            //}

            ////EnemyPlanets beillesztése
            //for (int i = 0; i < EnemyPlanets.Count; i++)
            //{
            //    EnemyPlanet item = EnemyPlanets[i];
            //    elements[(int)item.PosX, (int)item.PosY] = item;
            //}

            ////NeutralPlanetsList beillesztése
            //for (int i = 0; i < NeutralPlanets.Count; i++)
            //{
            //    NeutralPlanet item = NeutralPlanets[i];
            //    elements[(int)item.PosX, (int)item.PosY] = item;
            //}
        }

        ///// <summary>
        ///// Kiírja egy fájlba a PlanetMap aktuális tartalmát.
        ///// </summary>
        ///// <param name="fileName"></param>
        //public static void FajlbaIr(string fileName)
        //{
        //    using (System.IO.StreamWriter sw = new System.IO.StreamWriter(fileName))
        //    {
        //        for (int i = 0; i < elements.GetLength(0); i++)
        //        {
        //            for (int j = 0; j < elements.GetLength(1); j++)
        //            {
        //                if (elements[j, i] is NeutralPlanet)
        //                    sw.Write("N");
        //                else if (elements[j, i] is EnemyPlanet)
        //                    sw.Write("E");
        //                else if (elements[j, i] is OwnPlanet)
        //                    sw.Write("O");
        //                else
        //                    sw.Write("-");
        //            }
        //            sw.WriteLine();
        //        }
        //    }
        //}
    }
}