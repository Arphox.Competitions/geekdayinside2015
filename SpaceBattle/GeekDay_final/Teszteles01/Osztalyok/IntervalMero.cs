﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Teszteles01
{
    static class IntervalMero
    {
        static Stopwatch sw = new Stopwatch();
        static List<long> ElapsedTimes = new List<long>();

        public static void Mer()
        {
            if (AI1.Orajel % 3 == 0)
            {
                ElapsedTimes.Add(sw.ElapsedMilliseconds);
                Settings.INTERVAL = Convert.ToSingle(ElapsedTimes.Average());

                if (Settings.Naplozas.INTERVALS)
                {
                    using (StreamWriter writer = new StreamWriter(Settings.FilePaths.IntervalsTXT, true))
                    {
                        writer.WriteLine(Settings.INTERVAL);
                    }
                }
            }

            if (AI1.Orajel % 2 == 1)
                sw.Restart();
            else if (AI1.Orajel % 2 == 0)
                sw.Stop();
        }
    }
}