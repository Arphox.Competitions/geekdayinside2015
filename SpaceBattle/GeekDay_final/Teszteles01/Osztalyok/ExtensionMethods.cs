﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teszteles01
{
    static class ExtensionMethods
    {
        public static int OsszesNoU(this List<OwnPlanet> OwnPlanets)
        {
            int db = 0;
            for (int i = 0; i < OwnPlanets.Count; i++)
                db += OwnPlanets[i].NumberOfUnits;
            return db;
        }

        public static OwnPlanet Legnagyobb(this List<OwnPlanet> OwnPlanets)
        {
            int max = OwnPlanets.Max(y => y.NumberOfUnits);
            return (from x in OwnPlanets
                    where x.NumberOfUnits == max
                    select x).First();
        }
        public static OwnPlanet LegkozelebbiAmiNagyobbMint(this List<OwnPlanet> OwnPlanets, GameItem kozelMihez, int mennyinel)
        {
            if (OwnPlanets == null || OwnPlanets.Count == 0)
                return null;
            List<OwnPlanet> nagyobbak = OwnPlanets.Where(x => x.NumberOfUnits > mennyinel).ToList();

            if (nagyobbak.Count == 0)
                return null;

            int mini = 0;
            double minDist = double.MaxValue;

            for (int i = 1; i < nagyobbak.Count; i++)
            {
                double aktDist = Matek.KetPontTavolsaga(nagyobbak[i].PosX, nagyobbak[i].PosY, kozelMihez.PosX, kozelMihez.PosY);
                if (aktDist < minDist)
                {
                    minDist = aktDist;
                    mini = i;
                }
            }

            return nagyobbak[mini];
        }
    }
}