﻿using System.IO;

namespace Teszteles01
{
    class Init
    {
        public static void Inicializal()
        {
            if (File.Exists(Settings.FilePaths.CommandsTXT)) File.Delete(Settings.FilePaths.CommandsTXT);
            if (File.Exists(Settings.FilePaths.IntervalsTXT)) File.Delete(Settings.FilePaths.IntervalsTXT);
        }
    }
}