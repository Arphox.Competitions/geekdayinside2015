﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teszteles01
{
    static class ExtensionMethods
    {
        /// <summary>
        /// Megadja a legnagyobb OwnPlanet-et
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        /// 
        public static OwnPlanet Legnagyobb(this List<OwnPlanet> OwnPlanets)
        {
            int max = OwnPlanets.Max(y => y.NumberOfUnits);
            return (from x in OwnPlanets
                    where x.NumberOfUnits == max
                    select x).First();
        }
        public static OwnPlanet LegkozelebbiAmiNagyobbMint(this List<OwnPlanet> OwnPlanets, GameItem micsoda)
        {
            int mennyinel = micsoda.NumberOfUnits;

            if (OwnPlanets == null || OwnPlanets.Count == 0)
                return null;
            List<OwnPlanet> nagyobbak = OwnPlanets.Where(x => x.NumberOfUnits > mennyinel).ToList();

            if (nagyobbak.Count == 0)
                return null;

            int mini = 0;
            double minDist = double.MaxValue;

            for (int i = 1; i < nagyobbak.Count; i++)
            {
                double aktDist = Matek.KetPontTavolsaga(nagyobbak[i].PosX, nagyobbak[i].PosY, micsoda.PosX, micsoda.PosY);
                if (aktDist < minDist)
                {
                    minDist = aktDist;
                    mini = i;
                }
            }

            return nagyobbak[mini];
        }
        public static OwnPlanet LegkozelebbiAmiNagyobbMintEsNincsBenne(this List<OwnPlanet> OwnPlanets, GameItem micsoda, List<PointF> koordinatak)
        {
            int mennyinel = micsoda.NumberOfUnits;

            if (OwnPlanets == null || OwnPlanets.Count == 0)
                return null;
            List<OwnPlanet> nagyobbak = OwnPlanets
                .Where(x => x.NumberOfUnits > mennyinel
                            && !koordinatak.Contains(new PointF(x.PosX, x.PosY)))
                            .ToList();

            if (nagyobbak.Count == 0)
                return null;

            int mini = 0;
            double minDist = double.MaxValue;

            for (int i = 1; i < nagyobbak.Count; i++)
            {
                double aktDist = Matek.KetPontTavolsaga(nagyobbak[i].PosX, nagyobbak[i].PosY, micsoda.PosX, micsoda.PosY);
                if (aktDist < minDist)
                {
                    minDist = aktDist;
                    mini = i;
                }
            }

            return nagyobbak[mini];
        }

        /// <summary>
        /// Megadja azon OwnPlaneteket, amelyek nagyobbak, mint a megadott méret.
        /// </summary>
        /// <param name="items"></param>
        /// <param name="Meret"></param>
        /// <returns></returns>
        public static List<OwnPlanet> NagyobbakMint(this List<OwnPlanet> items, int Meret)
        {
            return (from x in items
                    where x.NumberOfUnits > Meret
                    select x).ToList();
        }
    }
}