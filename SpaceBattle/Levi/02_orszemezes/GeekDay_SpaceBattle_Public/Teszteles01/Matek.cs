﻿using System;

namespace Teszteles01
{
    static class Matek
    {
        public static double KetPontTavolsaga(float p1x, float p1y, float p2x, float p2y)
        {
            return Math.Sqrt(Math.Pow((p1x - p2x), 2) + Math.Pow((p1y - p2y), 2));
        }
        public static double KetItemTavolsaga(GameItem g1, GameItem g2)
        {
            return KetPontTavolsaga(g1.PosX, g1.PosY, g2.PosX, g2.PosY);
        }
    }
}
