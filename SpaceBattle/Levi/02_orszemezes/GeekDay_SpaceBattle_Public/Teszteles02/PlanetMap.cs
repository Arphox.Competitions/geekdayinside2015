﻿using System.Collections.Generic;
using System.Linq;

namespace Teszteles02
{
    static class PlanetMap
    {
        public static bool Initialized { get; private set; }

        static GameItem[,] elements = new GameItem[Settings.MapSizeX, Settings.MapSizeY];
        /// <summary>
        /// A mapon lévő bolygók.
        /// </summary>
        public static GameItem[,] Elements
        {
            get { return elements; }
        }

        //Constructor
        public static void Initialize(List<OwnPlanet> OwnPlanets, List<EnemyPlanet> EnemyPlanets, List<NeutralPlanet> NeutralPlanets)
        {
            //items beillesztése
            for (int i = 0; i < OwnPlanets.Count; i++)
            {
                OwnPlanet item = OwnPlanets[i];
                elements[(int)item.PosX, (int)item.PosY] = item;
            }

            //EnemyPlanets beillesztése
            for (int i = 0; i < EnemyPlanets.Count; i++)
            {
                EnemyPlanet item = EnemyPlanets[i];
                elements[(int)item.PosX, (int)item.PosY] = item;
            }

            //NeutralPlanetsList beillesztése
            for (int i = 0; i < NeutralPlanets.Count; i++)
            {
                NeutralPlanet item = NeutralPlanets[i];
                elements[(int)item.PosX, (int)item.PosY] = item;
            }

            Initialized = true;
        }

        //Properties
        public static int NeutralPlanetsCount
        {
            get
            {
                int db = 0;
                for (int i = 0; i < elements.GetLength(0); i++)
                {
                    for (int j = 0; j < elements.GetLength(1); j++)
                    {
                        if (elements[i, j] is NeutralPlanet)
                            db++;
                    }
                }
                return db;
            }
        }
        public static List<NeutralPlanet> NeutralPlanetsList
        {
            get
            {
                List<NeutralPlanet> lista = new List<NeutralPlanet>();

                for (int i = 0; i < elements.GetLength(0); i++)
                {
                    for (int j = 0; j < elements.GetLength(1); j++)
                    {
                        if (elements[i, j] is NeutralPlanet)
                            lista.Add((NeutralPlanet)elements[i, j]);
                    }
                }
                return lista;
            }
        }

        //Public Methods
        /// <summary>
        /// Aktualizálja a planetMapot.
        /// </summary>
        /// <param name="newMap"></param>
        public static void Refresh(List<OwnPlanet> OwnPlanets, List<EnemyPlanet> EnemyPlanets, List<NeutralPlanet> NeutralPlanets)
        {
            //items beillesztése
            for (int i = 0; i < OwnPlanets.Count; i++)
            {
                OwnPlanet item = OwnPlanets[i];
                elements[(int)item.PosX, (int)item.PosY] = item;
            }

            //EnemyPlanets beillesztése
            for (int i = 0; i < EnemyPlanets.Count; i++)
            {
                EnemyPlanet item = EnemyPlanets[i];
                elements[(int)item.PosX, (int)item.PosY] = item;
            }

            //NeutralPlanetsList beillesztése
            for (int i = 0; i < NeutralPlanets.Count; i++)
            {
                NeutralPlanet item = NeutralPlanets[i];
                elements[(int)item.PosX, (int)item.PosY] = item;
            }
        }

        /// <summary>
        /// Kiírja egy fájlba a PlanetMap aktuális tartalmát.
        /// </summary>
        /// <param name="fileName"></param>
        public static void FajlbaIr(string fileName)
        {
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(fileName))
            {
                for (int i = 0; i < elements.GetLength(0); i++)
                {
                    for (int j = 0; j < elements.GetLength(1); j++)
                    {
                        if (elements[j, i] is NeutralPlanet)
                            sw.Write("N");
                        else if (elements[j, i] is EnemyPlanet)
                            sw.Write("E");
                        else if (elements[j, i] is OwnPlanet)
                            sw.Write("O");
                        else
                            sw.Write("-");
                    }
                    sw.WriteLine();
                }
            }
        }
    }
}