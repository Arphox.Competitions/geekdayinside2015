﻿using System.Drawing;

namespace Teszteles01
{
    /// <summary>
    /// Statikus osztály a beállítások tárolására
    /// </summary>
    static class Settings
    {
        public static string ClientName = "RED";
        public static Brush ClientBrush = Brushes.Red;
        public static int MapSizeX = 0;
        public static int MapSizeY = 0;



        public static class FilePaths
        {
            public static string CommandsTXT = ClientName + "commands.txt";
            public static string PlanetMapTXT = ClientName + "planetMap.txt";
            public static string ScoutMapTXT = ClientName + "scoutMap.txt";
        }

        public static class FarShots
        {
            public static bool FAJLBA_NAPLOZAS = true;
        }
    }
}