﻿using SpaceBattle.Common;
using System.Collections.Generic;

namespace Teszteles01
{
    /// <summary>
    /// Feldolgozza a bejövő adatokat
    /// </summary>
    class Feldolgozo
    {
        List<GameItemDescriptor> gameItems;
        string sajatNev;

        public List<OwnPlanet> OwnPlanets { get; set; }
        public List<OwnShip> OwnShips { get; set; }
        public List<EnemyPlanet> EnemyPlanets { get; set; }
        public List<EnemyShip> EnemyShips { get; set; }
        public List<NeutralPlanet> NeutralPlanets { get; set; }

        /// <summary>
        /// Létrehozza az objektumot és legenerálja a listákat.
        /// </summary>
        /// <param name="gameItems"></param>
        /// <param name="sajatNev">ClientName</param>
        public Feldolgozo(List<GameItemDescriptor> gameItems, string sajatNev)
        {
            this.gameItems = gameItems;
            this.sajatNev = sajatNev;

            Generate();
        }

        /// <summary>
        /// Végigmegy a kapott gameItems-en és szortírozza az itemeket
        /// </summary>
        private void Generate()
        {
            OwnPlanets = new List<OwnPlanet>();
            OwnShips = new List<OwnShip>();
            EnemyPlanets = new List<EnemyPlanet>();
            EnemyShips = new List<EnemyShip>();
            NeutralPlanets = new List<NeutralPlanet>();

            for (int i = 0; i < gameItems.Count; i++)
            {
                if (GetItemType(gameItems[i]) == "OwnPlanet")
                    OwnPlanets.Add(OwnPlanetConverter(gameItems[i]));
                else if (GetItemType(gameItems[i]) == "OwnShip")
                    OwnShips.Add(OwnShipConverter(gameItems[i]));

                else if (GetItemType(gameItems[i]) == "EnemyPlanet")
                    EnemyPlanets.Add(EnemyPlanetConverter(gameItems[i]));
                else if (GetItemType(gameItems[i]) == "EnemyShip")
                    EnemyShips.Add(EnemyShipConverter(gameItems[i]));

                else
                    NeutralPlanets.Add(NeutralPlanetConverter(gameItems[i]));
            }
        }

        /// <summary>
        /// Átalakít egy GameItemDescriptor-t OwnPlanet-té, ha már biztos benne, hogy az. 
        /// </summary>
        /// <param name="nagyobbak"></param>
        /// <returns></returns>
        private OwnPlanet OwnPlanetConverter(GameItemDescriptor item)
        {
            OwnPlanet op = new OwnPlanet();

            op.IncTime = item.IncTime;
            op.ItemId = item.ItemId;
            op.NumberOfUnits = item.NumberOfUnits;
            op.PosX = item.PosX;
            op.PosY = item.PosY;

            return op;
        }

        /// <summary>
        /// Átalakít egy GameItemDescriptor-t OwnShip-pé, ha már biztos benne, hogy az. 
        /// </summary>
        /// <param name="nagyobbak"></param>
        /// <returns></returns>
        private OwnShip OwnShipConverter(GameItemDescriptor item)
        {
            OwnShip os = new OwnShip();

            os.DestinationX = item.DestinationX;
            os.DestinationY = item.DestinationY;
            os.ItemId = item.ItemId;
            os.NumberOfUnits = item.NumberOfUnits;
            os.PosX = item.PosX;
            os.PosY = item.PosY;

            return os;
        }

        /// <summary>
        /// Átalakít egy GameItemDescriptor-t EnemyPlanet-té, ha már biztos benne, hogy az. 
        /// </summary>
        /// <param name="nagyobbak"></param>
        /// <returns></returns>
        private EnemyPlanet EnemyPlanetConverter(GameItemDescriptor item)
        {
            EnemyPlanet ep = new EnemyPlanet();

            ep.IncTime = item.IncTime;
            ep.ItemId = item.ItemId;
            ep.NumberOfUnits = item.NumberOfUnits;
            ep.PlayerName = item.PlayerName;
            ep.PosX = item.PosX;
            ep.PosY = item.PosY;

            return ep;
        }

        /// <summary>
        /// Átalakít egy GameItemDescriptor-t EnemyShip-pé, ha már biztos benne, hogy az. 
        /// </summary>
        /// <param name="nagyobbak"></param>
        /// <returns></returns>
        private EnemyShip EnemyShipConverter(GameItemDescriptor item)
        {
            EnemyShip es = new EnemyShip();

            es.DestinationX = item.DestinationX;
            es.DestinationY = item.DestinationY;
            es.ItemId = item.ItemId;
            es.NumberOfUnits = item.NumberOfUnits;
            es.PlayerName = item.PlayerName;
            es.PosX = item.PosX;
            es.PosY = item.PosY;

            return es;
        }

        /// <summary>
        /// Átalakít egy GameItemDescriptor-t NeutralPlanet-té, ha már biztos benne, hogy az. 
        /// </summary>
        /// <param name="nagyobbak"></param>
        /// <returns></returns>
        private NeutralPlanet NeutralPlanetConverter(GameItemDescriptor item)
        {
            NeutralPlanet np = new NeutralPlanet();

            np.IncTime = item.IncTime;
            np.ItemId = item.ItemId;
            np.NumberOfUnits = item.NumberOfUnits;
            np.PosX = item.PosX;
            np.PosY = item.PosY;

            return np;
        }

        /// <summary>
        /// Megadja az paraméterként megadott GameItemDescriptor típusát. 
        /// A típus lehet: NeutralPlanet, OwnPlanet, OwnShip, EnemyPlanet és EnemyShip.
        /// </summary>
        /// <param name="nagyobbak"></param>
        /// <returns></returns>
        private string GetItemType(GameItemDescriptor item)
        {
            if (item.PlayerName == string.Empty)
                return "NeutralPlanet";
            if (item.PlayerName == sajatNev) //Own...
            {
                if (item.ItemType == "Planet")
                    return "OwnPlanet";
                else
                    return "OwnShip";
            }
            else //Enemy...
            {
                if (item.ItemType == "Planet")
                    return "EnemyPlanet";
                else
                    return "EnemyShip";
            }
        }
    }
}